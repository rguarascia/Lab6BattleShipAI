# Lab6BattleShipAI - by Ryan G & Hayden M
This was our final lab of semester 3 at Mohawk College for data structures and algorithms. The full lab description is down below.
TL;DR: Make a bot using the school API that plays 10,000 games of battleship while taking at least as many shots as possible.

`10,000 game avg: 56 shots/per game`


### Final Mark
110%

## Lab instructions
#### Background

You are to write a program that will play a limited game of battleship. The game of battleship is typically played with two players, each of which place 5 ships of various sizes on a 10 x 10 grid. Each player on a turn by turn basis attempts to place a shot where the opponent has placed a ship. Of course, your opponent can not see where you have placed your ships but must strategically call out shots to try and find and sink each of your ships. The player that can sink all of the opponent ships first is the winner. For a more complete description see Battleship

In this instance of BattleShip the computer will randomly place 5 ships (lengths 2,3,3,4,5) on the board. A total of 17 spaces on the board out of 100 will have a ship. Once you obtain 17 hits you have solved the game. Your goal is to achieve the lowest average number of shots to do this. There are many different strategies to solve this problem.

The starting code provided includes the BattleShip API. The Battleship API is described below:

public BattleShip() - you need to call the constructor once in your program to create an instance of the battleship game.
public boolean shoot(Point shot) - you need to call this function to make each shot. See the sample source code for an example use.
public int numberOfShipsSunk() - returns the total number of ships sunk at any point during the game. It is a good idea to use this method to determine when a ship has been sunk.
public boolean allSunk() - returns a boolean value that indicates whether all the ships have been sunk.
public int totalShotsTaken() - returns the total number of shots taken. Your code needs to be responsible for ensuring the same shot is not taken more than once.
public int[] shipSizes() - returns an ArrayList of all of the ship sizes. The length of the array indicates how many ships are present.  This arrayList is fixed for the game.  It does not update when a ship is sunk.  You must try to write logic in your code to determine which ship has been sunk.
public enum CellState - this enum object is very useful for marking cells has either Empty, Hit or Miss. It also has a convenience toString method so that can be used for printing purposes. You may also create your own Enum / Class for this in your code, but it is suggested that you use this instead of integers / characters to mark a Cell state
Steps

Download the starting code for the project from the eLearn dropbox. Rename the project to Comp10152_Lab6
Create a class for your battleShip Logic - A starter solution is in the starting code for the lab.
Add some logic to your class to eliminate duplicate cell selection (i.e. - don't fire on the same cell twice)
Timing code is in the starting solution. You need to leave that code in your solution. It is best to continue to play 10000 games to get a reasonable number of games in order to calculate the average. Do not use less games.
Some Ideas for solving the problem:
Consider reducing the number of shots you need to fire to find a ship.
Create a map that tracks where you have placed your shots so that you don't fire on a cell more than once.
After you have hit a ship, you may want to change how you select your next shot.
The probability of a ship being on a particular square is not the same for each square. This probability changes after each shot as well. This technique for shot selection is a little more advanced but yields better ship targeting and when used correctly can greatly reduce the number of shots required.
Here's a good strategy link: http://www.datagenetics.com/blog/december32011/
