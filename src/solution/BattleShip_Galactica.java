package solution;

import battleship.BattleShip;
import java.awt.Point;
import java.util.HashSet;
import java.util.Random;

/**
 * Our bot first shots the first four spots, then shots in a checker-pattern outward. We has have fallback points if for when it reaches an index 
 * without all the ship sunk. When we found a ship, we enter destoryMode where we should Up, Down, Left, Right till the ship has been destory.
 * 
 * @author Hayden Meloche (000363920) and Ryan Guarascia (000379166)
 */
public class BattleShip_Galactica {
    private int gameSize;
    private BattleShip battleShip;
    private Random random;
    private Point fallbackHits[] = {new Point(5, 5), new Point(5, 3), new Point(6, 4), new Point(6, 6), new Point(4, 4), new Point(4, 6), new Point(3, 3), new Point(3, 7), new Point(7, 4), new Point(7, 6)}; //These are the center pieces which we should be hitting first.
    HashSet<Point> shotMap = new HashSet<Point>();
    HashSet<Point> shotsHit = new HashSet<Point>();
    boolean debug = false;

    /**
     * Constructor keeps a copy of the BattleShip instance
     *
     * @param b previously created battleship instance - should be a new game
     */
    public BattleShip_Galactica(BattleShip b) {
        battleShip = b;
        gameSize = b.BOARDSIZE;
        random = new Random();   // Needed for random shooter - not required for more systematic approaches
    }
    int x;
    int y;
    int max = 100;
    int min = 0;
    //Our checkboard pattern shot listing.
    Point[] myPoints = {
        new Point(4, 5),
        new Point(5, 4),
        //inside square
        new Point(5, 6),
        new Point(6, 5),
        new Point(6, 3),
        new Point(4, 3),
        new Point(3, 4),
        new Point(3, 6),
        //next layer
        new Point(3, 2),
        new Point(5, 2),
        new Point(7, 2),
        new Point(7, 4),
        new Point(7, 6),
        new Point(6, 7),
        new Point(4, 7),
        new Point(2, 7),
        new Point(2, 5),
        new Point(2, 3),
        //next layer
        new Point(2, 1),
        new Point(4, 1),
        new Point(6, 1),
        new Point(8, 1),
        new Point(8, 3),
        new Point(8, 5),
        new Point(8, 7),
        new Point(7, 8),
        new Point(5, 8),
        new Point(3, 8),
        new Point(1, 8),
        new Point(1, 6),
        new Point(1, 4),
        new Point(1, 2),
        //next layer
        new Point(1, 0),
        new Point(3, 0),
        new Point(5, 0),
        new Point(7, 0),
        new Point(9, 0),
        new Point(0, 9),
        new Point(2, 9),
        new Point(4, 9),
        new Point(6, 9),
        new Point(8, 9),
        new Point(0, 7),
        new Point(0, 5),
        new Point(0, 3),
        new Point(0, 1),
        new Point(9, 8),
        new Point(9, 6),
        new Point(9, 4),
        new Point(9, 2)};
    int count = 0;

    /**
     * This method fires your cannon shots to the ships
     * @return weather the shot was a hit or miss
     */
    public boolean fireShot() {
        double x = myPoints[0].getX();
        Point newShot = nextShot();
        shotMap.add(newShot);
        Point shot = newShot;
        boolean hit = battleShip.shoot(shot);
        if (hit == true) {
            if (debug) {
                System.out.println("found ship after " + battleShip.totalShotsTaken() + " shots \n"
                        + "Ship was hit at " + shot);
            }
            if (!shotsHit.contains(newShot)) {
                shotsHit.add(newShot);
            }
            DestroyMode();
        }
        return hit;
    }
    /**
     * First our preset shots, then checkerboard then worst case, random
     * @return the next shots that will be taken 
     */
    public Point nextShot() {
        x = (int) (new Point(myPoints[count]).getX());
        y = (int) (new Point(myPoints[count]).getY());
        while (shotMap.contains(new Point(myPoints[count]))) {
            int index = 0;
            if (count == 48) {
                while (true) {
                    x = (int) (new Point(fallbackHits[index]).getX());
                    y = (int) (new Point(fallbackHits[index]).getY());
                    while (shotMap.contains(new Point(x, y))) {
                        index++;
                        if (index > 9) {
                            break;
                        }
                        x = (int) (new Point(fallbackHits[index]).getX());
                        y = (int) (new Point(fallbackHits[index]).getY());
                    }
                    if (index > 9) {
                        break;
                    }
                    Point newShot = new Point(x, y);
                    shotMap.add(newShot);
                    Point shot = newShot;
                    boolean hit = battleShip.shoot(shot);
                    if (hit == true) {
                        if (debug) {
                            System.out.println("found ship after " + battleShip.totalShotsTaken() + " shots \n"
                                    + "Ship was hit at " + shot);
                        }
                        if (!shotsHit.contains(newShot)) {
                            shotsHit.add(newShot);
                        }
                        DestroyMode();
                    }
                }
            }
            if (count <= 48) {
                count++;
                x = (int) (new Point(myPoints[count]).getX());
                y = (int) (new Point(myPoints[count]).getY());
            } else {
                if (debug) {
                    System.err.println("RANDOM INITIATED");
                }
                x = random.nextInt(gameSize);
                y = random.nextInt(gameSize);
                while (shotMap.contains(new Point(x, y))) {
                    x = random.nextInt(gameSize);
                    y = random.nextInt(gameSize);
                }
                break;
            }
        }
        if (debug) {
            System.out.println(count);
        }
        return new Point(x, y);
    }

    /**
     * Fires your shots to the board method.
     * @param x current x
     * @param y current y
     * @return weather we should change direction
     */
    public boolean fireCannon(int x, int y) {
        boolean nextDir = false;
        Point shot = new Point(x, y);
        if (debug) {
            System.out.println("shooting spot" + shot);
        }
        boolean result = battleShip.shoot(shot);
        if (result == true) {
            if (debug) {
                System.out.println("shot worked!");
            }
        }
        shotMap.add(shot);
        if (result == false) {
            nextDir = true;
        } else if (!shotsHit.contains(shot)) {
            shotsHit.add(shot);
        }
        return nextDir;
    }

    /**
     * Fires Up, Down, Left, Right until the ship is sunk
     */
    public void DestroyMode() {
        int numberOfShipsSunk = battleShip.numberOfShipsSunk();
        int ystart = y;
        int xstart = x;
        boolean tryUp = false;
        boolean tryDown = false;
        boolean tryLeft = false;
        boolean tryRight = false;
        int counter1 = 1;
        int counter2 = 1;
        int counter3 = 1;
        int counter4 = 1;
        while (true) {
            if (tryUp == false) {
                if (debug) {
                    System.out.println("shooting up");
                }
                y = ystart + counter1;
                counter1++;
                if (y <= 9) {
                    if (!shotMap.contains(new Point(x, y))) {
                        tryUp = fireCannon(x, y);
                        if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                            if (debug) {
                                System.out.println("SHIP SUNK");
                            }
                            break;
                        }
                    } else if (shotMap.contains(new Point(x, y)) && shotsHit.contains(new Point(x, y))) {
                        y = ystart + counter1++;
                        if (debug) {
                            System.out.println("Shooting up 2");
                        }
                        if (y >= 9) {
                            tryUp = true;
                        } else if (!shotMap.contains(new Point(x, y))) {
                            tryUp = fireCannon(x, y);
                            if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                                if (debug) {
                                    System.out.println("SHIP SUNK");
                                }
                                break;
                            }
                        }
                    }
                } else {
                    tryUp = true;
                    if (debug) {
                        System.out.println("exiting try up");
                    }
                }
            } else if (tryDown == false && tryUp == true) {
                if (debug) {
                    System.out.println("shooting down");
                }
                y = ystart - counter2;
                counter2++;
                if (y > -1) {
                    if (!shotMap.contains(new Point(x, y))) {
                        tryDown = fireCannon(x,y);
                        if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                            if (debug) {
                                System.out.println("SHIP SUNK");
                            }
                            break;
                        }
                    } else if (shotMap.contains(new Point(x, y)) && shotsHit.contains(new Point(x, y))) {
                        y = ystart - counter2++;
                        if (debug) {
                            System.out.println("Shooting down 2");
                        }
                        if (y > -1 && (!shotMap.contains(new Point(x, y)))) {
                            tryDown = fireCannon(x,y);
                            if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                                if (debug) {
                                    System.out.println("SHIP SUNK");
                                }
                                break;
                            }
                        } else {
                            tryDown = true;
                        }
                    }
                } else {
                    tryDown = true;
                    if (debug) {
                        System.out.println("exiting try down");
                    }
                }
            } else if (tryLeft == false && tryUp == true && tryDown == true) {
                if (debug) {
                    System.out.println("shooting left");
                }
                y = ystart;
                x = xstart - counter3;
                counter3++;
                if (x > -1) {
                    if (!shotMap.contains(new Point(x, y))) {
                        tryLeft = fireCannon(x,y);
                        if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                            if (debug) {
                                System.out.println("SHIP SUNK");
                            }
                            break;
                        }
                    } else if (shotMap.contains(new Point(x, y)) && shotsHit.contains(new Point(x, y))) {
                        y = ystart;
                        if (debug) {
                            System.out.println("Shooting left 2");
                        }
                        x = xstart - counter3++;
                        if (x > -1 && (!shotMap.contains(new Point(x, y)))) {
                           tryLeft = fireCannon(x,y);
                            if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                                //if (debug) System.out.println("SHIP SUNK");
                                break;
                            }
                        } else {
                            tryLeft = true;
                        }
                    }
                } else {
                    tryLeft = true;
                    if (debug) {
                        System.out.println("exited try left");
                    }
                }
            } else if (tryRight == false && tryUp == true && tryDown == true && tryLeft == true) {
                if (debug) {
                    System.out.println("shooting right");
                }
                y = ystart;
                x = xstart + counter4;
                if (debug) {
                    System.out.println("counter 4 = " + counter4);
                }
                if (debug) {
                    System.out.println("x = " + x);
                }
                counter4++;
                if (x <= 9) {
                    if (!shotMap.contains(new Point(x, y))) {
                        tryRight = fireCannon(x,y);
                        if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                            if (debug) {
                                System.out.println("SHIP SUNK");
                            }
                            break;
                        }
                    } else if (shotMap.contains(new Point(x, y)) && shotsHit.contains(new Point(x, y))) {
                        y = ystart;
                        x = xstart + counter4++;
                        if (debug) {
                            System.out.println("Shooting right 2");
                        }
                        if (x <= 9 && (!shotMap.contains(new Point(x, y)))) {
                             tryRight = fireCannon(x,y);
                            if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                                if (debug) {
                                    System.out.println("SHIP SUNK");
                                }
                                break;
                            }
                        } else {
                            tryRight = true;
                        }
                    } else {
                        tryRight = true;
                        if (debug) {
                            System.out.println("exited try right");
                        }
                    }
                } else if (numberOfShipsSunk != battleShip.numberOfShipsSunk()) {
                    if (debug) {
                        System.out.println("SHIP SUNK");
                    }
                    break;

                } else {
                    if (debug) {
                        System.out.println("didn't find the ship");
                    }
                    break;
                }
            }
            if (debug) {
                System.out.println("end of loop.");
            }
            if (debug) {
                System.out.println("" + tryUp + tryDown + tryLeft + tryRight);
            }
            if (tryUp == true && tryDown == true && tryLeft == true && tryRight == true) {
                break;
            }
        }
    }
}
